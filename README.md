# TeamClassifier

## But

Permettre de créer des équipes équilibrées en fonction de divers critères.
Créer à l'origine pour équilibrer les équipes pendant les tournois Rocket League sur le serveur de Ponce (streamer français), en utilisant les critères suivants : 
- nombre d'heure de jeu
- rang en 2v2
- rang en 3v3

## Etapes

Les étapes décrites sont des cas particuliers pour un tournoi Rocket League.

### Données

Fichier CSV avec : 
- nom des joueurs
- nombre d'heure de jeu
- rang en 2v2 (numérique)
- rang en 3v3 (numérique)

Le numéro des colonnes est réglable avec des constantes dans le [CSVReader.java](https://gitlab.com/Lucas_Raynaud/teamclassifier/-/blob/main/src/teamClassifier/CSVReader.java#L12).

### Calculs

Pour obtenir des équipes équilibrées, il a été calculé des coefficients de [regression linéaire](https://fr.wikipedia.org/wiki/R%C3%A9gression_lin%C3%A9aire) qui permettent de pondérer, de la manière la plus juste en fonction des résultats des précédents tournois, les différents [critères de classifications](https://gitlab.com/-/ide/project/Lucas_Raynaud/teamclassifier/edit/main/-/README.md#donn%C3%A9es) des joueurs. <br>
Ces coefficients sont obtenus à l'aide de la bibliothèque [scikit-learn](https://scikit-learn.org/stable/). <br>
Scipt disponible [ici](https://gitlab.com/Lucas_Raynaud/teamclassifier/-/blob/main/RL_Tournament.py).

Un score est calculé pour chaque joueurs, puis une moyenne de tous ces scores est aussi calculée. <br>
Pour plus de détails voir la fonction [`calculerScore(int, int, int)`](https://gitlab.com/Lucas_Raynaud/teamclassifier/-/blob/main/src/teamClassifier/CSVReader.java#L44).

### Création des équipes

#### 3v3

Pour créer les équipes :
- trie des joueurs en fonction de leur score
- séléction de 3 joueurs et calcul de la moyenne de leurs scores
- l'équipe présentant la moyenne s'approchant le plus de la moyenne globale est gardée
- supression de ces trois joueurs de la liste

#### 2v2

Pour créer les équipes :
- sélection d'un joueur
- recherche du joueur avec lequel la moyenne de l'équipe sera la plus proche de la moyenne globale
- supression de ces deux joueurs de la liste

### Récupération des équipes

Un fichier CSV est générer avec toutes les informations des équipes (numéro d'équipe et informations sur les joueurs).
[Chemin du fichier](https://gitlab.com/Lucas_Raynaud/teamclassifier/-/blob/main/src/teamClassifier/TeamClassifier.java#L9) réglable.

## Contact

Pour me contacter : 
- discord : heliorr#9542
