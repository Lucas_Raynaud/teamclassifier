#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd

classement = pd.read_csv("Tournoi_RL_Janvier_2022.csv")


# In[2]:


classement.head()


# In[3]:


classement.info()


# In[4]:


classement["Moyenne heure"] = classement["Moyenne heure"].apply(lambda x: float(x.split(',')[0]))
classement['Moyenne rang 2v2'] = classement['Moyenne rang 2v2'].apply(lambda x: float(x.split(',')[0]))
classement['Moyenne rang 3v3'] = classement['Moyenne rang 3v3'].apply(lambda x: float(x.split(',')[0]))


# In[5]:


classement.info()
classement.head()


# In[6]:


feature_name = ["Moyenne heure", "Moyenne rang 3v3", "Moyenne rang 2v2"]
target_name = "Classement"
data, target = classement[feature_name], classement[target_name]


# In[7]:


from sklearn.linear_model import LinearRegression

linear_regression = LinearRegression()
linear_regression.fit(data,target)


# In[8]:


linear_regression.coef_


# In[9]:


linear_regression.intercept_


# In[ ]:




