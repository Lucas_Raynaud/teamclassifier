package teamClassifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class TeamClassifier {
	
	private static final String PATHFILELISTEJOUEUR = "D:\\JavaWorkspace\\TeamClassifier\\ListeJoueur.csv";
	private static final String PATHFILERESULT3V3 = "D:\\JavaWorkspace\\TeamClassifier\\dataResult3v3.csv";
	private static final String PATHFILERESULT2V2 = "D:\\JavaWorkspace\\TeamClassifier\\dataResult2v2.csv";
	public static final String PATH = "D:\\JavaWorkspace\\TeamClassifier\\data.csv";

	public static void main(String[] args) {

		ArrayList<Joueur> listeJoueurs = CSVReader.read(PATH);
		ArrayList<Joueur> listeJoueursTriee = listeJoueurs;
		Collections.sort(listeJoueursTriee);

		double avg = avg(listeJoueurs);

		System.out.println("Moyenne : " + avg);

		ArrayList<Equipe> listeEquipes = new ArrayList<Equipe>();

		String choix;

		System.out.println("2v2 : 2 \n3v3 : 3 \nAfficher liste joueurs : 4");
		try (Scanner sc = new Scanner(System.in)) {
			choix = sc.nextLine();
		}
		switch (choix) {
		case "2": {
			create2v2Team(listeJoueursTriee, avg, listeEquipes);
			CSVWriter.generateCsvFile(PATHFILERESULT2V2, listeEquipes);
			break;
		}
		case "3": {
			create3v3Team(listeJoueursTriee, avg, listeEquipes);
			CSVWriter.generateCsvFile(PATHFILERESULT3V3, listeEquipes);
			break;
		}
		case "4": {
			System.out.println(listeJoueursTriee);
			CSVWriter.generateCsvListeJoueur(PATHFILELISTEJOUEUR, listeJoueursTriee);
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + choix);
		}
	}

	private static void create3v3Team(ArrayList<Joueur> listeJoueurs, double avg, ArrayList<Equipe> listeEquipes) {
		if(!listeEstVide(listeJoueurs)) {
			ArrayList<Joueur> listeJoueursTirees = new ArrayList<Joueur>();
			listeEquipes.clear();
			double teamAvg;
			double closestDiff;
			Joueur closestMate1, closestMate2;
			
			for (Joueur joueur1 : listeJoueurs) {
				if (joueurPasDansLaListe(listeJoueursTirees, joueur1)) {
					closestDiff = Double.POSITIVE_INFINITY;
					closestMate1 = null;
					closestMate2 = null;
					for (Joueur joueur3 : listeJoueurs) {
						if (joueursSontDifferents(joueur3, joueur1) && joueurPasDansLaListe(listeJoueursTirees, joueur3)) {
							for (Joueur joueur2 : listeJoueurs) {
								if (joueursSontDifferents(joueur1, joueur2) && joueursSontDifferents(joueur3, joueur2) && joueurPasDansLaListe(listeJoueursTirees, joueur2)) {
									teamAvg = (joueur1.getScore() + joueur2.getScore() + joueur3.getScore()) / 3;
									if (Math.abs(avg - teamAvg) < closestDiff) {
										closestMate2 = joueur2;
										closestMate1 = joueur3;
										closestDiff = Math.abs(avg - teamAvg);
									}
								}
							}
						}
					}
					listeJoueursTirees.add(closestMate1);
					listeJoueursTirees.add(closestMate2);
					listeJoueursTirees.add(joueur1);
					listeEquipes.add(new Equipe3v3(joueur1, closestMate1, closestMate2));
				}
			}
			System.out.println(listeEquipes);
		}
	}

	/**
	 * @param listeJoueurs
	 * @return
	 */
	private static boolean listeEstVide(ArrayList<Joueur> listeJoueurs) {
		return listeJoueurs.isEmpty();
	}

	/**
	 * @param listeJoueursTirees
	 * @param joueur2
	 * @return
	 */
	private static boolean joueurPasDansLaListe(ArrayList<Joueur> listeJoueursTirees, Joueur joueur2) {
		return !listeJoueursTirees.contains(joueur2);
	}

	/**
	 * @param joueur1
	 * @param joueur2
	 * @return
	 */
	private static boolean joueursSontDifferents(Joueur joueur1, Joueur joueur2) {
		return !joueur2.equals(joueur1);
	}

	private static void create2v2Team(ArrayList<Joueur> listeJoueurs, double avg, ArrayList<Equipe> listeEquipes) {
		if (!listeEstVide(listeJoueurs)) {
			listeEquipes.clear();
			ArrayList<Joueur> listeJoueursTemp = listeJoueurs;
			while (!listeEstVide(listeJoueursTemp)) {
				listeEquipes.add(new Equipe2v2(listeJoueursTemp.get(0), listeJoueursTemp.get(listeJoueursTemp.size()-1)));
				listeJoueursTemp.remove(0);
				listeJoueursTemp.remove(listeJoueursTemp.size()-1);
			}
			System.out.println(listeEquipes);
		}
	}

	public static double avg(ArrayList<Joueur> listeJoueurs) {
		double sum = 0;
		for (Joueur joueur : listeJoueurs) {
			sum += joueur.getScore();
		}
		return sum/listeJoueurs.size();
	}

}
