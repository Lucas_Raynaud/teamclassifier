package teamClassifier;

import java.util.HashMap;
import java.util.Objects;

public class Joueur implements Comparable<Joueur>{

	private static HashMap<Integer, String> rangs = new HashMap<>();
	static {
		rangs.put(1, "Bronze I");
		rangs.put(2, "Bronze II");
		rangs.put(3, "Bronze III");
		rangs.put(4, "Argent I");
		rangs.put(5, "Argent II");
		rangs.put(6, "Argent III");
		rangs.put(7, "Or I");
		rangs.put(8, "Or II");
		rangs.put(9, "Or III");
		rangs.put(10, "Platine I");
		rangs.put(11, "Platine II");
		rangs.put(12, "Platine III");
		rangs.put(13, "Diamant I");
		rangs.put(14, "Diamant II");
		rangs.put(15, "Diamant III");
		rangs.put(16, "Champion I");
		rangs.put(17, "Champion II");
		rangs.put(18, "Champion III");
		rangs.put(19, "Grand Champion I");
		rangs.put(20, "Grand Champion II");
		rangs.put(21, "Grand Champion III");
		rangs.put(22, "Super Sonic Legend");
	}

	private String nom;
	private int nbHeuresJeu;
	private int rang2v2;
	private int rang3V3;
	private double score;

	public Joueur(String nom,int nbHeuresJeu, int rang2v2, int rang3v3, double score) {
		super();
		this.nom = nom;
		this.nbHeuresJeu = nbHeuresJeu;
		this.rang2v2 = rang2v2;
		this.rang3V3 = rang3v3;
		this.score = score;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNbHeuresJeu() {
		return nbHeuresJeu;
	}

	public void setNbHeuresJeu(int nbHeuresJeu) {
		this.nbHeuresJeu = nbHeuresJeu;
	}

	public int getRang2v2() {
		return rang2v2;
	}

	public void setRang2v2(int rang2v2) {
		this.rang2v2 = rang2v2;
	}

	public int getRang3V3() {
		return rang3V3;
	}

	public void setRang3V3(int rang3v3) {
		rang3V3 = rang3v3;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return nom + " nbHeuresJeu=" + nbHeuresJeu + " rang2v2=" + rangs.get(this.rang2v2) + " rang3v3=" + rangs.get(this.rang3V3) + " score=" + this.score + "\n";

	}

	@Override
	public int hashCode() {
		return Objects.hash(nom);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Joueur other = (Joueur) obj;
		return Objects.equals(nom, other.nom);
	}

	@Override
	public int compareTo(Joueur o) {
		if (getScore() < o.getScore()) {
			return -1;
		}
		else if (getScore() > o.getScore()) {
			return 1;
		}
		return 0;
	}
}