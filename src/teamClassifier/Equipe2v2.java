package teamClassifier;

public class Equipe2v2 extends Equipe {

	public Equipe2v2(Joueur joueur1, Joueur joueur2) {
		super(joueur1, joueur2);
		// TODO Auto-generated constructor stub
	}
	
	public double calculMoyenne() {
		return (getJoueur1().getScore() + getJoueur2().getScore()) / 2;
	}

	@Override
	public String toString() {
		return "\nEquipe : \nJoueur1 : " + getJoueur1() + "Joueur2 : " + getJoueur2() + "score : " + calculMoyenne();
	}
}
