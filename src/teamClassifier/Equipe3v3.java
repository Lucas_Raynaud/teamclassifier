package teamClassifier;

public class Equipe3v3 extends Equipe {
	
	private Joueur joueur3;
	
	public Equipe3v3(Joueur joueur1, Joueur joueur2, Joueur joueur3) {
		super(joueur1, joueur2);
		this.joueur3 = joueur3;
		// TODO Auto-generated constructor stub
	}

	public Joueur getJoueur3() {
		return joueur3;
	}

	public void setJoueur3(Joueur joueur3) {
		this.joueur3 = joueur3;
	}
	
	@Override
	public String toString() {
		return "\nEquipe : \nJoueur1 : " + getJoueur1() + "Joueur2 : " + getJoueur2() + "Joueur3 : " + getJoueur3() + "score : " + calculMoyenne();
	}

	private double calculMoyenne() {
		return (getJoueur1().getScore() + getJoueur2().getScore() + getJoueur3().getScore()) / 3;
	}

}
