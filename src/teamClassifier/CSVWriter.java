package teamClassifier;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class CSVWriter {

	public static void generateCsvListeJoueur(String fileName, ArrayList<Joueur> listeJoueur) {
		
		try {
			FileWriter delete = new FileWriter(fileName);
			delete.close();
			FileWriter writer = new FileWriter(fileName);
			writer.append("Nom");
			writer.append(',');
			writer.append("Nb heures jeu");
			writer.append(',');
			writer.append("Rang 2v2");
			writer.append(',');
			writer.append("Rang 3v3");
			writer.append(',');
			writer.append("score");
			writer.append('\n');
			for (Joueur joueur : listeJoueur) {
				writer.append(joueur.getNom());
				writer.append(',');
				writer.append(String.valueOf(joueur.getNbHeuresJeu()));
				writer.append(',');
				writer.append(String.valueOf(joueur.getRang2v2()));
				writer.append(',');
				writer.append(String.valueOf(joueur.getRang3V3()));
				writer.append(',');
				writer.append(String.valueOf(joueur.getScore()));
				writer.append('\n');
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void generateCsvFile(String fileName, ArrayList<Equipe> listeEquipes)
	{
		try
		{
			FileWriter delete = new FileWriter(fileName);
			delete.close();
			FileWriter writer = new FileWriter(fileName);
			
			int compteur = 1;

			writer.append("Equipe");
			writer.append(',');
			writer.append("Nom");
			writer.append(',');
			writer.append("Nb heures jeu");
			writer.append(',');
			writer.append("Rang 2v2");
			writer.append(',');
			writer.append("Rang 3v3");
			writer.append(',');
			writer.append("score");
			writer.append('\n');

			for (Equipe equipe : listeEquipes) {	
				writer.append(String.valueOf(compteur));
				writer.append(',');
				writer.append(equipe.getJoueur1().getNom());
				writer.append(',');
				writer.append(String.valueOf(equipe.getJoueur1().getNbHeuresJeu()));
				writer.append(',');
				writer.append(String.valueOf(equipe.getJoueur1().getRang2v2()));
				writer.append(',');
				writer.append(String.valueOf(equipe.getJoueur1().getRang3V3()));
				writer.append(',');
				writer.append(String.valueOf(equipe.getJoueur1().getScore()));
				writer.append('\n');
				writer.append(String.valueOf(compteur));
				writer.append(',');
				writer.append(equipe.getJoueur2().getNom());
				writer.append(',');
				writer.append(String.valueOf(equipe.getJoueur2().getNbHeuresJeu()));
				writer.append(',');
				writer.append(String.valueOf(equipe.getJoueur2().getRang2v2()));
				writer.append(',');
				writer.append(String.valueOf(equipe.getJoueur2().getRang3V3()));
				writer.append(',');
				writer.append(String.valueOf(equipe.getJoueur2().getScore()));
				writer.append('\n');
				if (equipe.getClass() == Equipe3v3.class) {
					writer.append(String.valueOf(compteur));
					writer.append(',');
					writer.append(((Equipe3v3) equipe).getJoueur3().getNom());
					writer.append(',');
					writer.append(String.valueOf(((Equipe3v3) equipe).getJoueur3().getNbHeuresJeu()));
					writer.append(',');
					writer.append(String.valueOf(((Equipe3v3) equipe).getJoueur3().getRang2v2()));
					writer.append(',');
					writer.append(String.valueOf(((Equipe3v3) equipe).getJoueur3().getRang3V3()));
					writer.append(',');
					writer.append(String.valueOf(((Equipe3v3) equipe).getJoueur3().getScore()));
					writer.append('\n');
				}
				compteur++;
			}

			writer.flush();
			writer.close();
		} catch(IOException e) {
			e.printStackTrace();
		} 
	}

}
