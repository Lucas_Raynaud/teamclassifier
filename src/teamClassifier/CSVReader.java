package teamClassifier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public abstract class CSVReader {
		
	public final static double COEFFHEURE = -2.00e-03;
	public final static double COEFF3V3 = -4.81;
	public final static double COEFF2V2 = -3.10;
	public final static double INTERCEPT = 131.15;
	
	public final static int COLONNENOM = 0;
	public final static int COLONNENBHEURES = 1;
	public final static int COLONNE2V2 = 5;
	public final static int COLONNENOM3V3 = 3;
	
	public static ArrayList<Joueur> read(String chemin) {
		Path path = Paths.get(chemin);
		
		List<String> lines = null;
		
		try {
			lines = Files.readAllLines(path);
		} catch (IOException e) {
			System.out.println(e);
			System.out.println("Erreur lecture fichier");
		}
		ArrayList<Joueur> listeJoueurs = new ArrayList<>();
		for (String string : lines) {
			String[] split = string.split(",");
			int nbHeuresJeu = Integer.parseInt(split[COLONNENBHEURES]);
			int rang2v2 = Integer.parseInt(split[COLONNE2V2]);
			int rang3v3 = Integer.parseInt(split[COLONNENOM3V3]);
			listeJoueurs.add(new Joueur(split[COLONNENOM], nbHeuresJeu, rang3v3, rang2v2, calculerScore(nbHeuresJeu,rang2v2,rang3v3)));
		}
		return listeJoueurs;
	}
	
	public static double calculerScore(int nbHeure, int rang2v2, int rang3v3) {
		return nbHeure*COEFFHEURE + rang2v2*COEFF2V2 + rang3v3*COEFF3V3 + INTERCEPT;
	}
}